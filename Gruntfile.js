module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    appconfig: {
      dist: 'malharianelly-132579527'
    },
    watch: {
      all: {
        files: ['app/**/*'],
        tasks: ['default'],
        options: {
          spawn: false,
        },
      },
    },
    run: {
      theme: {
        cwd: '<%= appconfig.dist %>',
        cmd: 'theme',
        args: [
          'upload',
          '*'
        ]
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      js: {
        src: 'app/assets/**/*.js',
        dest: '<%= appconfig.dist %>/assets/<%= pkg.name %>.min.js'
      }
    },
    cssmin: {
      concat: {
        files: [{
          '<%= appconfig.dist %>/assets/<%= pkg.name %>.css': ['app/assets/**/*.css']
        }]
      }
    },
    copy: {
      assets: {
        expand: true,
        cwd: 'app/assets',
        src: [ '**' ],
        dest: '<%= appconfig.dist %>/assets/',
        flatten: true,
        filter: 'isFile',
      },
      all: {
        expand: true,
        cwd: 'app',
        src: [ '**', '!**/assets/**' ],
        dest: '<%= appconfig.dist %>/',
      },
    },
    clean: {
      dist: ["<%= appconfig.dist %>"]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-run');

  // Default task(s).
  grunt.registerTask('default', [
    'copy'
  ]);

  // Default task(s).
  grunt.registerTask('cleancopy', [
    'clean:dist',
    'copy',
    'cssmin'
  ]);

  // Default task(s).
  grunt.registerTask('publish', [
    'clean:dist',
    'copy',
    'cssmin',
    'uglify'
  ]);

};
