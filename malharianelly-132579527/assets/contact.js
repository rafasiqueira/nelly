// CONTACT MAP

var PageContact = function() {

	var _init = function() {

		var mapbg = new GMaps({
			div: '#gmapbg',
			lat: 41.3122372,
			lng: 16.3128139,
			scrollwheel: false
		});


		mapbg.addMarker({
			lat: 41.3122372,
			lng: 16.3128139,
			title: 'Your Location',
			infoWindow: {
				content: '<h3>Apulia Textiles Ltd.</h3><p>Via dell’ Euro n.4, 76121 Barletta (BT), Puglia, Italy</p>'
			}
		});
	}

    return {
        //main function to initiate the module
        init: function() {

            _init();

        }

    };
}();

$(document).ready(function() {
    PageContact.init();
});
